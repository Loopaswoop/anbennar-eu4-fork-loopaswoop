namespace = jadd_empire

# Seat of the Court?
country_event = {
	id = jadd_empire.1
	title = jadd_empire.1.t
	desc = jadd_empire.1.d
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		tag = F51
	}
	
	#Keep the court in its current location
	option = {
		name = jadd_empire.1.a
		trigger = { 
			any_owned_province = {
				has_province_modifier = jaddempire_court_seat
			}
		}
		if = {
			limit = { capital = 643 }
			add_ruler_modifier =  {
				name = jaddempire_azka_sur_capital
				duration = -1
			}
		}
		else_if = {
			limit = { capital = 601 }
			add_ruler_modifier =  {
				name = jaddempire_bulwar_capital
				duration = -1
			}
		}
		else_if = {
			limit = { capital = 2909 }
			add_ruler_modifier =  {
				name = jaddempire_religious_capital
				duration = -1
			}
		}
		capital_scope = {
			random_list = {
				1 = { add_base_tax = 1}
				1 = { add_base_production = 1}
				1 = { add_base_manpower = 1}
			}
		}
	}
	
	#Move the court to Azka-Sur
	option = {
		name = jadd_empire.1.b
		trigger = {
			NOT = { 643 = { has_province_modifier = jaddempire_court_seat } }
			owns_core_province = 643
			643 = { controlled_by = ROOT }
		}
		hidden_effect = {
			every_owned_province = { #remove the province modifiers from the previous court
				remove_province_modifier = jaddempire_court_seat
				remove_province_modifier = jaddempire_court_area
			}
		}
		add_ruler_modifier =  {
			name = jaddempire_azka_sur_capital
			duration = -1
		}
		643 = {
			add_province_modifier = {
				name = jaddempire_court_seat
				duration = -1
			}
			move_capital_effect = yes
			random_list = {
				1 = { add_base_tax = 1}
				1 = { add_base_production = 1}
				1 = { add_base_manpower = 1}
			}
		}
		azka_sur_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		upper_suran_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		eduz_vacyn_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		panuses_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		grixek_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		jikarzax_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
	}
	
	#Move the court to Bulwar
	option = {
		name = jadd_empire.1.c
		trigger = {
			NOT = { 601 = { has_province_modifier = jaddempire_court_seat } }
			owns_core_province = 601
			601 = { controlled_by = ROOT }
		}
		hidden_effect = {
			every_owned_province = { #remove the province modifiers from the previous court
				remove_province_modifier = jaddempire_court_seat
				remove_province_modifier = jaddempire_court_area
			}
		}
		add_ruler_modifier =  {
			name = jaddempire_bulwar_capital
			duration = -1
		}
		601 = {
			add_province_modifier = {
				name = jaddempire_court_seat
				duration = -1
			}
			move_capital_effect = yes
			random_list = {
				1 = { add_base_tax = 1}
				1 = { add_base_production = 1}
				1 = { add_base_manpower = 1}
			}
		}
		bulwar_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		west_naza_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		harklum_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		middle_suran_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
	}
	
	#Move the court to religious capital
	option = {
		name = jadd_empire.1.e
		trigger = {
			NOT = { 2909 = { has_province_modifier = jaddempire_court_seat } }
			owns_core_province = 2909
			2909 = { controlled_by = ROOT }
		}
		hidden_effect = {
			every_owned_province = { #remove the province modifiers from the previous court
				remove_province_modifier = jaddempire_court_seat
				remove_province_modifier = jaddempire_court_area
			}
		}
		add_ruler_modifier =  {
			name = jaddempire_religious_capital
			duration = -1
		}
		2909 = {
			add_province_modifier = {
				name = jaddempire_court_seat
				duration = -1
			}
			move_capital_effect = yes
			random_list = {
				1 = { add_base_tax = 1}
				1 = { add_base_production = 1}
				1 = { add_base_manpower = 1}
			}
		}
		areab34_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
		areab35_area = {
			limit = { owned_by = ROOT }
			add_province_modifier = {
				name = jaddempire_court_area
				duration = -1
			}
		}
	}
}