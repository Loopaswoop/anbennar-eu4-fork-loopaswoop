namespace = zokka_jaddari_war

#Zokka-Jaddari Conflict
# country_event = {
	# id = zokka_jaddari_war.1
	# title = zokka_jaddari_war.1.t
	# desc = zokka_jaddari_war.1.d #After our recent war against the sun elves we have pushed directly into Bulwar. However our rear flank hasn't been covered yet
	# picture = HORDE_ON_HORSEBACK_eventPicture
	
	# fire_only_once = yes
	# major = yes
	
	# trigger = {
		# tag = F29
		# is_year = 1445
	# }
	
	# mean_time_to_happen = {
		# months = 12
	# }
	
	# option = {
		# name = zokka_jaddari_war.1.a  #We must crush these elves
		# ai_chance = {
			# factor = 1
			# modifier = {
				# factor = 0
				# is_at_war = yes
			# }
			# modifier = {
				# factor = 0
				# has_any_disaster = yes
			# }
		# }
		# ROOT = {
			# declare_war_with_cb = {
				# who = F46
				# casus_belli = cb_monster_vs_civ
			# }
		# }
		# areab32_area = {
			# add_claim = F29
		# }
		# areab39_area = {
			# add_claim = F29
		# }
		# areab40_area = {
			# add_claim = F29
		# }
	# }
	# option = {
		# name = zokka_jaddari_war.1.b #They're not a threat
		# ai_chance = {
			# factor = 0
		# }
		# add_prestige = -10
	# }
# }

#Zokka-Jaddar duel: Zokka Falls
country_event = {
	id = zokka_jaddari_war.2
	title = zokka_jaddari_war.2.t
	desc = zokka_jaddari_war.2.d
	picture = HORDE_ON_HORSEBACK_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.2.a  #Victorious!
		
		add_prestige = 10
		F29 = { country_event = { id = zokka_jaddari_war.3 days = 1 } }
		
		ai_chance = {
			factor = 1
		}
	}
}

#Zokka-Jaddar duel: Zokka Falls (notification)
country_event = {
	id = zokka_jaddari_war.3
	title = zokka_jaddari_war.3.t
	desc = zokka_jaddari_war.3.d
	picture = HORDE_ON_HORSEBACK_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.3.a  #Disastrous!
		
		kill_ruler = yes
		add_stability = -2
		country_event = {
			id = zokka_jaddari_war.6
			days = 60
			random = 120
		}
		F46 = {
			country_event = {
				id = zokka_jaddari_war.8
				days = 60
				random = 120
			}
		}
		
		ai_chance = {
			factor = 1
		}
	}
}

#Zokka-Jaddar duel: Jaddar Falls
country_event = {
	id = zokka_jaddari_war.4
	title = zokka_jaddari_war.4.t
	desc = zokka_jaddari_war.4.d
	picture = HORDE_ON_HORSEBACK_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.4.a  #Victorious!
		
		add_prestige = 10
		F46 = { country_event = { id = zokka_jaddari_war.5 days = 1 } }
		
		ai_chance = {
			factor = 1
		}
	}
}

#Zokka-Jaddar duel: Jaddar Falls (Notification)
country_event = {
	id = zokka_jaddari_war.5
	title = zokka_jaddari_war.5.t
	desc = zokka_jaddari_war.5.d
	picture = HORDE_ON_HORSEBACK_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.5.a  #Disastrous!
		
		kill_ruler = yes
		add_stability = -2
		country_event = {
			id = zokka_jaddari_war.7
			days = 60
			random = 120
		}
		
		ai_chance = {
			factor = 1
		}
	}
}

#Zokka-Jaddari war: Tluukt absorbs Zokka's pack, Hasr becomes independent and goes to follow the Jadd. 
country_event = {
	id = zokka_jaddari_war.6
	title = zokka_jaddari_war.6.t
	desc = zokka_jaddari_war.6.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.6.a #Offer to join Tluukt
		ai_chance = {
			factor = 5
		}
		
		F28 = {
			country_event = {
				id = zokka_jaddari_war.10
				days = 3
			}
		}
	}
	
	option = {
		name = zokka_jaddari_war.6.b #Fight on, it's not yet the end
		ai_chance = {
			factor = 1
		}
	}
}

#Zokka-Jaddari war: Elayenna takes in Jaddari refugees, the rest of the land falls to the gnolls
country_event = {
	id = zokka_jaddari_war.7
	title = zokka_jaddari_war.7.t
	desc = zokka_jaddari_war.7.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.7.a #Seek refuge with Elayenna
			
		switch_tag = F49
		2909 = { cede_province = F49 add_core = F49 }
		2905 = { cede_province = F49 add_core = F49 }
		2907 = { cede_province = F49 add_core = F49 }
		
		ai_chance = {
			factor = 5
		}
	}
	
	option = {
		name = zokka_jaddari_war.7.b #Fight on, it's not yet the end
			
		ai_chance = {
			factor = 0
		}
	}
}

#Zokka-Jaddari war: Hasr's independence
country_event = {
	id = zokka_jaddari_war.8
	title = zokka_jaddari_war.8.t
	desc = zokka_jaddari_war.8.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		NOT = { exists = F45 }
	}
	
	immediate = {
		hidden_effect = {
			upper_suran_area = {
				limit = { owned_by = F29 }
				cede_province = F45
				add_core = F45
			}
			F29 = {
				country_event = { id = zokka_jaddari_war.9 days = 1}
			}
		}
	}
	
	option = {
		name = zokka_jaddari_war.8.a #Offer aid
		
		add_manpower = -5
		vassalize = F45
		F45 = {
			change_religion = the_jadd
			capital_scope = { change_religion = the_jadd }
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		ai_chance = {
			factor = 1
		}
	}
	
	option = {
		name = zokka_jaddari_war.8.b #Refuse aid
		
		add_prestige = -10
		F45 = {
			declare_war_with_cb = {
				who = F29
				casus_belli = cb_independence_war
			}
		}
		
		ai_chance = {
			factor = 0
		}
	}
}

#Zokka-Jaddari war: Hasr's independence (Notification)
country_event = {
	id = zokka_jaddari_war.9
	title = zokka_jaddari_war.9.t
	desc = zokka_jaddari_war.9.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.9.a
		
		ai_chance = {
			factor = 1
		}
	}
}

#Zokka-Jaddari war: Tluukt absorbs Zokka's pack?
country_event = {
	id = zokka_jaddari_war.10
	title = zokka_jaddari_war.10.t
	desc = zokka_jaddari_war.10.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = zokka_jaddari_war.10.a #Yes
		ai_chance = {
			factor = 1
		}
		
		F29 = { switch_tag = F28 }
		vassalize = F29
		capital_scope = { infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT }
		add_manpower = 10
	}
	
	option = {
		name = zokka_jaddari_war.10.b #No, we don't want the weak
		ai_chance = {
			factor = 0
		}
		
		F29 = {
			country_event = {
				id = zokka_jaddari_war.11
				days = 3
			}
		}
	}
}

#Zokka-Jaddari war: Tluukt offers no help
country_event = {
	id = zokka_jaddari_war.11
	title = zokka_jaddari_war.11.t
	desc = zokka_jaddari_war.11.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = { #Well fuck them then
		name = zokka_jaddari_war.11.a
		
		ai_chance = {
			factor = 1
		}
		
		create_general = { tradition = 40 }
	}
}
