government = monarchy
add_government_reform = plutocratic_reform
government_rank = 1
primary_culture = white_reachman
religion = regent_court
technology_group = tech_cannorian
capital = 701
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1424.3.10 = {
	monarch = {
		name = "Patrik II"
		dynasty = "of Esald"
		birth_date = 1402.1.17
		adm = 0
		dip = 0
		mil = 5
	}
}