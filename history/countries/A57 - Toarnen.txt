government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = arannese
religion = regent_court
technology_group = tech_cannorian
capital = 922
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.1 = {
	monarch = {
		name = "Ad�la�de"
		dynasty = "s�l na Loop"	#From Loopuis in Roilsard
		birth_date = 1410.7.2
		adm = 3
		dip = 1
		mil = 3
		female = yes
	}
}

1422.1.1 = { set_country_flag = lilac_wars_rose_party }