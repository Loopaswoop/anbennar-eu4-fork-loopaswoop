government = republic
add_government_reform = merchants_reform
government_rank = 1
primary_culture = royal_harimari
add_accepted_culture = rasarhid
religion = high_philosophy
technology_group = tech_harimari
religious_school = silk_turban_school
capital = 4500

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }