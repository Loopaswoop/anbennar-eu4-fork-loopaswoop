


culture = cave_goblin
religion = great_dookan
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 2
base_manpower = 1
native_size = 4
native_ferocity = 5
native_hostileness = 5
add_permanent_province_modifier = {
	name = flooded_province
	duration = -1
}